<?php

namespace Lableb\ApiTask;

class Services
{
    // public function greet(String $sName)
    // {
    //     return 'Hi ' . $sName . '! How are you doing today?';
    // }
    private $BASE_URL = "https://api-bahuth.lableb.com/api/v1/";
    public $PROJECT_NAME;
    public $COLLECTION_NAME;
    public $SEARCH_QUERY;
    public $SEARCH_TOKEN;
    public $HANDLER;
    
    public $SESSION_ID;
    public $FEEDBACK_URL;

    public $PARAMS;


    function __construct(
        $__PROJECT_NAME = 'wptest',
        $__COLLECTION_NAME = 'posts',
        $__SEARCH_TOKEN = '',
        $__HANDLER='default'
    ) {
        $this->PROJECT_NAME =   $__PROJECT_NAME;
        $this->COLLECTION_NAME = $__COLLECTION_NAME;
        $this->SEARCH_TOKEN =   $__SEARCH_TOKEN;
        $this->HANDLER =   $__HANDLER;
    }

    // GETTERS FUNCTIONS
    public function get_handler()
    {
        return $this->HANDLER;
    }
    public function get_project_name()
    {
        return $this->PROJECT_NAME;
    }
    public function get_collaction_name()
    {
        return $this->COLLECTION_NAME;
    }
    function get_search_query()
    {
        return $this->SEARCH_QUERY;
    }
    function get_search_token()
    {
        return $this->SEARCH_TOKEN;
    }

    function get_sessio_id()
    {
        return $this->SESSION_ID;
    }    
    function get_feedback_url()
    {
        return $this->FEEDBACK_URL;
    }
    //SETTERS FUNCTIONS
    public function set_project_name($__PROJECT_NAME = '')
    {
        $this->PROJECT_NAME = $__PROJECT_NAME;
    }
    public function set_collaction_name($__COLLECTION_NAME = '')
    {
        $this->COLLECTION_NAME = $__COLLECTION_NAME;
    }
    function set_search_query($__SEARCH_QUERY = '')
    {
        $this->SEARCH_QUERY = $__SEARCH_QUERY;
    }
    function set_search_token($__SEARCH_TOKEN = '')
    {
        $this->SEARCH_TOKEN = $__SEARCH_TOKEN;
    }

    function set_handler($__HANDLER = 'default')
    {
        $this->HANDLER = $__HANDLER;
    }
    function set_sessio_id($__SESSION_ID)
    {
        $this->SESSION_ID = $__SESSION_ID;

    }    
    function set_feedback_url($__FEEDBACK_URL)
    {
        $this->FEEDBACK_URL = $__FEEDBACK_URL;
    }




    // APIS
    public function search()
    {
        $url = $this->BASE_URL . $this->PROJECT_NAME . "/collections/" . $this->COLLECTION_NAME . "/search/". $this->HANDLER;

        $this->PARAMS = array(
            'q' => $this->SEARCH_QUERY,
            'cat' => 'Lifestyle',
            'limit' => 1,
            'token' => $this->SEARCH_TOKEN
        );
        // var_dump($url);
        // var_dump($this->PARAMS);
        // die;
        $result = $this->CallAPI('GET', $url, $this->PARAMS);
        // $res=strval($result);
        return $result;
    }
    public function feedback()
    {
        $url =$this->BASE_URL . $this->PROJECT_NAME ."/collections/" .$this->COLLECTION_NAME . "/search/".$this->HANDLER."/feedback/hits";

        $this->PARAMS = array(
            'q'         => $this->SEARCH_QUERY,
            'item_id'   => 'Lifestyle',
            'item_order'=> 1,
            'token'     => $this->SEARCH_TOKEN,
            'url'       =>$this->FEEDBACK_URL,//'http://mysite.com/posts/lableb-post',
            'session_id'=>$this->SESSION_ID//'1c4Hb23'
        );

        
        // var_dump($url);
        // var_dump($this->PARAMS);
        // die;

        $result = $this->CallAPI('GET', $url, $this->PARAMS);
        return $result;
    }

    // Method: POST, PUT, GET etc
    // Data: array("param" => "value") ==> index.php?param=value
    public function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}

