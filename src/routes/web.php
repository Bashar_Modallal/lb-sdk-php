<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'=>'Lableb\ApiTask\Http\Controllers'],function(){
    Route::get('/greet/{name}/greet/{name2}', 'LablebController@greet');
    Route::group(['prefix' => '/api/v1/{project}/collections/{collection}/search/{handler}'], function()  
    {  
        Route::get('/feedback/hits', 'LablebController@feedback');
        Route::get('/', 'LablebController@search');
    });
});



