<?php

namespace Lableb\ApiTask\Http\Controllers;

use App\Http\Controllers\controller;
use Lableb\ApiTask\Services as LB;
use Illuminate\Http\Request;

class LablebController extends Controller
{
    public function greet(Request $request ,$name1, $name2)
    {
        return response()->json(['success' => true, 'data' => "Hello1: ".$name1 .' - Hello2: ' .$name2,  'message' => 'Lableb Api Service']);
    }


    public function search(Request $request , $PROJECT_NAME='wptest' , $COLLACTION_NAME='posts' ,$HANDLER ='default'){
        $LB = new LB($PROJECT_NAME ,
                     $COLLACTION_NAME,'',$HANDLER);

        // $LB->set_project_name('wptest');
        // $LB->set_collaction_name('posts');
        if($request->has('token'))
            $LB->set_search_token( $request->get('token'));
        else 
            return response()->json(['success' => false, 'data' =>'Token Is Missing' ,  'message' => 'Lableb Api Service -Paramaters are Missing']);

        if($request->has('q'))
            $LB->set_search_query($request->get('q'));
        else 
            $LB->set_search_query('');
        return response()->json(['success' => true, 'data' =>$LB->search() ,  'message' => 'Lableb Api Service -Search']);
    }

    public function feedback(Request $request , $PROJECT_NAME='wptest' , $COLLACTION_NAME='posts' ,$HANDLER ='default'){
        // Request $request ,$PROJECT_NAME,$COLLECTION_NAME
        // $LB = new LB();
        $LB = new LB($PROJECT_NAME ,
                     $COLLACTION_NAME,'',
                     $HANDLER);

        if($request->has('token'))
            $LB->set_search_token( $request->get('token'));
        else 
            return response()->json(['success' => false, 'data' =>'Token Is Missing' ,  'message' => 'Lableb Api Service -Paramaters are Missing']);

        if($request->has('q'))
            $LB->set_search_query($request->get('q'));
        else 
            $LB->set_search_query('');

            
        if($request->has('url'))
            $LB->set_feedback_url($request->get('url'));
        else 
            return response()->json(['success' => false, 'data' =>'Feedback Url Is Missing' ,  'message' => 'Lableb Api Service -Paramaters are Missing']);


        if($request->has('session_id'))
            $LB->set_sessio_id($request->get('session_id'));
        else 
            return response()->json(['success' => false, 'data' =>'Feedback session_id Is Missing' ,  'message' => 'Lableb Api Service -Paramaters are Missing']);
        
        return response()->json(['success' => true, 'data' =>$LB->feedback() ,  'message' => 'Lableb Api Service -Search']);
    }
}
