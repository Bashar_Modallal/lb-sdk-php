# LB Cloud Search Laravel sdk

## Installation

Install using composer:

add to your composer.json 
```php
    "require": {
        "lableb/api_task": "^1.0.0"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/Bashar_Modallal/lb-sdk-php"
        }
    ],
```
Then Execute In Your Terminal :  

```
$ composer update 
```

## Usage

To Use Default Route ,  Add Service Provider To The Following Path config/app.php  
```php
    'providers' => [

       /*
         * Package Service Providers...
       */
       Lableb\ApiTask\LablebServiceProvider::class
    ]
```

Then you can initialize the sdk as follows:  

```php
use Lableb\ApiTask\Services as LB;

$LB = new LB('PROJECT NAME' ,
             'Collaction Name',
             'Token',
             'Handler');
// You Can Use The Following Functions To Set Your Parameters

//Setters
$LB->set_search_token( 'token');
$LB->set_search_query('Search Query');            
$LB->set_feedback_url('Website Url'); // For Feedback Search
$LB->set_sessio_id('Session Id ');  // For Feedback Search


//Getters
$LB->get_search_token( 'token');
$LB->get_search_query('Search Query');            
$LB->get_feedback_url('Website Url'); // For Feedback Search
$LB->get_sessio_id('Session Id ');  // For Feedback Search
```

### Notice  

The Following Routes Are Actually Added To Your Routes **(If You Added LablebServiceProvider in config/app.php File)**

**Search**  
**GET** /api/v1/{project}/collections/{collection}/search/{handler}/feedback/hits?  
**Parameters**

| query string | Description                  | Required | Example                                   |
| ------------ | ---------------------------- | -------- | ----------------------------------------- |
| q            | Search Query                 | no       | 'Lableb'                                  |
| url          | url of the source document   | yes      | 'https://fun-funfood.com/2018/06/pizza' |
| session_id   | how many documents to fetch  | yes      | 1c4Hb23                                   |
| token        | a unique user id             | yes      | qxDFI791xxxx-8wmxIBIONYiEK44PGnxxxxxx     |

------------------------------------------------------------------------------------------------------

**Feedback Search**  
**GET** /api/v1/{PROJECT_NAME}/collections/{COLLACTION_NAME}/search/{HANDLER}?  
**Parameters**

| query string   | Description                  | Required | Example                                   |
| ------------   | ---------------------------- | -------- | ----------------------------------------- |
| q              | Search Query                 | no       | 'Lableb'                                  |
| token          | a unique user id             | yes      | qxDFI791xxxx-8wmxIBIONYiEK44PGnxxxxxx     |

**Common**  

| key            | Description                  | Required  | Example                                  |
| ------------   | ---------------------------- | -------- | ----------------------------------------- |
| PROJECT_NAME   | Project Name                 | yes       | 'wptest'                                 |
| COLLACTION_NAME| Collaction Name              | yes       | 'posts'                                  |
| HANDLER        |                              | no        | 'default'                                |

------------------------------------------------------------------------------------------------------
